package org.ldv.sio;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ClientTest {

    private Client c;

    @Before
    public void initializeEachTest() {

        Adresse domicile = new Adresse("14 rue principale", "Leudon en Brie", "77320");
        Adresse domicile2 = new Adresse("17 rue du chateau", "Melun", "77000");
        Adresse domicile3 = new Adresse("70 King Street", "Franklin Springs", "30360");
        ArrayList<Adresse> list = new ArrayList<>();
        list.add(domicile);
        list.add(domicile2);
        list.add(domicile3);
        this.c = new Client("Dijkstra", "Edsger", null);
    }

    @Test
    public void getNom() {
        assertEquals("Dijkstra", this.c.getNom());
    }

    @Test
    public void setNom() {
        this.c.setNom(this.c.getNom().toUpperCase());
        assertEquals("DIJKSTRA", this.c.getNom());
    }

    @Test
    public void getPrenom() {
        assertEquals("Edsger", this.c.getPrenom());
    }

    @Test
    public void setPrenom() {
        this.c.setPrenom(this.c.getPrenom().toUpperCase());
        assertEquals("EDSGER", this.c.getPrenom());
    }

    @Test
    public void bonneAdresseDomicile() {
        Adresse domicile = new Adresse("3 rue du clocher", "Melun", "77000");
        ArrayList comparaison = this.c.getLivraison();
        assertEquals(comparaison, domicile);
    }
    @Test
    public void adresseLivraisonNulle() {

    }


}