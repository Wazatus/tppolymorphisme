package org.ldv.sio;

import java.util.ArrayList;

public class Client {
    private String nom;
    private String prenom;
    private Adresse domicile;
    private ArrayList<Adresse> livraison = null;


    public Client(String nom, String prenom, Adresse domicile) {
        this.nom = nom;
        this.prenom = prenom;
        this.domicile = domicile;
        ArrayList<Adresse> livraison = new ArrayList<>();
        livraison.add(domicile);
        this.livraison = livraison;
    }

    public Client(String nom, String prenom, Adresse domicile, ArrayList<Adresse> livraison) {
        this.nom = nom;
        this.prenom = prenom;
        this.domicile = domicile;
        this.livraison = livraison;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public ArrayList<Adresse> getLivraison() {
        return livraison;
    }

    public void setLivraison(ArrayList<Adresse> livraison) {
        this.livraison = livraison;
    }

    public Adresse getDomicile() {
        return domicile;
    }

    public void setDomicile(Adresse domicile) {
        this.domicile = domicile;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", domicile=" + domicile +
                ", livraison=" + livraison +
                '}';
    }
}
