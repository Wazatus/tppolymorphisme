package org.ldv.sio;

import java.util.Objects;

public class AdresseEtendue {
    private String email;
    private String url;

    public AdresseEtendue(String email, String url) {
        this.email = email;
        this.url = url;
    }

    public AdresseEtendue() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdresseEtendue that = (AdresseEtendue) o;
        return email.equals(that.email) &&
                url.equals(that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, url);
    }

    @Override
    public String toString() {
        return "AdresseEtendue{" +
                "email='" + email + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
