package org.ldv.sio;


import java.util.ArrayList;


public class Main {

    private static Object Adresse;

    public static void main(String[] args) {
        Adresse domicile = new Adresse("14 rue principale", "Leudon en Brie", "77320");
        Adresse domicile2 = new Adresse("17 rue du chateau", "Melun", "77000");
        Adresse domicile3 = new Adresse("70 King Street", "Franklin Springs", "30360");
        ArrayList<Adresse> list = new ArrayList<>();
        list.add(domicile);
        list.add(domicile2);
        list.add(domicile3);
        Client c = new Client("Racz","Stan", domicile, list);
        System.out.println(c.toString());
    }
}
